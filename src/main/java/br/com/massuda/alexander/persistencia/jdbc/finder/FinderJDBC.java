package br.com.massuda.alexander.persistencia.jdbc.finder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;


public class FinderJDBC<T> extends FinderJDBCUtils<T> {

	public FinderJDBC(Class<T> entidade) {
		super(entidade);
	}

	public FinderJDBC(Class<T> entidade, TipoOperacao tipo) {
		super(entidade);
		this.tipoOperacao = tipo;
	}

	public T pesquisar(Long id) {
		T o = null;
		try {
			String sql = GeradorSQLBeanFactory.get(entidade).getComandoSelecao();

			sql += "\n where id = ?";

			if (tipoOperacao == TipoOperacao.NORMAL) {
				try {
					comandoPreparado = novoComandoPreparado(sql);
					comandoPreparado.setLong(1, id);

					resultados = pesquisarComParametros(comandoPreparado);
					if (resultados.next()) {
						o = (T) novaInstancia();
						preencher(o);
					}
				} catch (SQLSyntaxErrorException e) {
					criarTabela(e);
					return pesquisar(id);
				} catch (SQLException e) {
					if (e instanceof SQLSyntaxErrorException) {
						criarTabela((SQLSyntaxErrorException) e);
					}
					return pesquisar(id);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						fecharObjetosDeComandoEPesquisa(null, comandoPreparado, resultados);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} else {
				PreparedStatement comandoPreparado = null;
				ResultSet resultado = null;
				try {
					comandoPreparado = novoComandoPreparado(sql);
					comandoPreparado.setLong(1, id);

					resultado = pesquisarComParametros(comandoPreparado);

					if (resultado.next()) {
						o = (T) novaInstancia();
						preencher(o, resultado);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						fecharObjetosDeComandoEPesquisa(null, comandoPreparado, resultado);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return o;
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public List<T> pesquisarPorNomeComo(String nome) {
		List<T> objs = new ArrayList<>();
		StringBuilder sql = new StringBuilder(GeradorSQLBeanFactory.get(entidade).getComandoSelecao());
		sql.append(" where nome like ?");

		try {
			comandoPreparado = novoComandoPreparado(sql.toString());
			comandoPreparado.setString(1, "%"+nome+"%");
			System.out.println(comandoPreparado);
			resultados = pesquisarComParametros(comandoPreparado);

			while (resultados.next()) {
				T u = novaInstancia();
				preencher(u);
				objs.add(u);
			}
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			return pesquisarPorNomeComo(nome);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objs;
	}

	public List<T> listar() {
		List<T> objs = new ArrayList<T>();
		String sql = GeradorSQLBeanFactory.get(entidade).getComandoSelecao();
		try {
			resultados = pesquisarSemParametro(sql);
			while (resultados.next()) {
				T o = novaInstancia();
				preencher(o);
				objs.add(o);
			}
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			return listar();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println(objs);
		return objs;
	}

	public List<T> pesquisar(List<Filtro> filtros) {
		List<T> objs = new ArrayList<T>();
		String sql = GeradorSQLBeanFactory.get(entidade).getComandoSelecao();
		try {
			sql += parametrizarPesquisa(filtros);
			comandoPreparado = novoComandoPreparado(sql);
			preencherParametrosDeComandoComFiltros(comandoPreparado, filtros);
			resultados = pesquisarComParametros(comandoPreparado);

			while (resultados.next()) {
				T o = novaInstancia();
				preencher(o);
				objs.add(o);
			}
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			return pesquisar(filtros);
		} catch (SQLException e) {
			System.err.println(sql);

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return objs;
	}
	
	public ResultSet pesquisar(Object o, Field campo) throws SQLSyntaxErrorException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		String sql = null;
		try {
			Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
			String atributoNome = new StringBuilder(o.getClass().getSimpleName().toLowerCase()).append("_").append("id").toString();
			Long id = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o, null);
			sql = GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoSelecao();
			List<Filtro> filtros = new ArrayList<Filtro>();
			filtros.add(new Filtro(Long.class, atributoNome, TipoFiltro.IGUAL_A, id, true));
			
			return pesquisar(sql, filtros);
		} catch (SQLSyntaxErrorException e) {
			System.err.println(sql);
			try {
				criarTabela(o, campo);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return pesquisar(o, campo);
		} finally {
			if (TipoOperacao.NORMAL == tipoOperacao) {
				try {
					fecharObjetosDeComandoEPesquisa(comando, comandoPreparado, null);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void criarTabela(Object o, Field campo) throws SQLException {
		Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		String sqlTabelaCriacao = new StringBuilder(GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoTabelaCriacao()).toString();
		executarComando(sqlTabelaCriacao);
	}

	public ResultSet pesquisar(String sql, List<Filtro> filtros) throws SQLSyntaxErrorException {
		try {
			sql += parametrizarPesquisa(filtros);
			comandoPreparado = novoComandoPreparado(sql);
			preencherParametrosDeComandoComFiltros(comandoPreparado, filtros);
			resultados = pesquisarComParametros(comandoPreparado);
			return resultados;
		} catch (SQLException e) {
			System.err.println(sql);
			if (e instanceof SQLSyntaxErrorException) {
				throw new SQLSyntaxErrorException(e);
			}
			e.printStackTrace();
		} finally {
			if (TipoOperacao.NORMAL == tipoOperacao) {
				try {
					fecharObjetosDeComandoEPesquisa(comando, comandoPreparado, null);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
