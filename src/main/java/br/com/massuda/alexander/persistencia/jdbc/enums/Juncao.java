/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.enums;

/**
 * @author Alex
 *
 */
public enum Juncao {

	NORMAL,
	INVERSA;
	
}
