/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc;

/**
 * @author Alex
 *
 */
public enum Movimento {
	// Determinando code para cada enum
    Spock(1),
    Lagarto(2),
    Pedra(3),
    Tesoura(4),
    Papel(5);

    // Codigo do tipo enumerado
    private int code;
    // Construtor do tipo enumerado
    private Movimento(int code) {
        this.code = code;
    }
    // Getter
    public int getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }

    // Metodo estatico para converter um valor numero para o tipo enumerado
    public static Movimento valueOf(int code) {
        for (Movimento value : Movimento.values()) {
            if (value.getCode() == code) {
                return value;
            }
        }
        throw new IllegalArgumentException("Invalid OrderStatus Code");
    }
}
