/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author Alex
 *
 */
public class ComunicacaoTipoFloat extends ComunicacaoJDBC {

	public ComunicacaoTipoFloat() {
		this.SQL_VALOR_NULO = Types.FLOAT;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setFloat(indice, 		(existe(valor) ? (Float) valor : null));
	}

	@Override
	void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado)
			throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		setter.invoke(o, resultado.getFloat(campo.getName()));
	}

}
