/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.waiso.framework.abstratas.Classe;

/**
 * @author Alex
 *
 */
public abstract class ComunicacaoJDBC extends Classe {
	
	public int SQL_VALOR_NULO = 0;
	
	abstract void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException;
	abstract void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

}
