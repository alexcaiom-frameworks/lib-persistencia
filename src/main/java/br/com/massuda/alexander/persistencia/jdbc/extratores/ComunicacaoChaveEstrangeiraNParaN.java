/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;

/**
 * @author Alex
 *
 */
public class ComunicacaoChaveEstrangeiraNParaN extends ComunicacaoJDBC {
	
	private static final String REGEX_PONTO = "\\.";

	public ComunicacaoChaveEstrangeiraNParaN() {
		this.SQL_VALOR_NULO = Types.VARCHAR;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(String) valor);
	}

	public void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<Long> listaEntidadeFraca = getChavesEstrangeiras(resultado, o, campo);
		setLinhasDaChaveEstrangeira(listaEntidadeFraca, o, campo, setter);
	}
	

	private void setLinhasDaChaveEstrangeira(List<Long> listaEntidadeFraca, Object o, Field campo, Method setter) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		List<Filtro> filtros = new ArrayList<>();
		
		if (ObjectUtils.isNotEmpty(listaEntidadeFraca)) {
			for (Long chave : listaEntidadeFraca) {
				filtros.add(new Filtro(Long.class, "id", TipoFiltro.EM, chave, true));
			}
			
			FinderJDBC<?> finder = new FinderJDBC<>(classeGenerica);
			List<?> fks = finder.pesquisar(filtros);
			setter.invoke(o, fks);
		}
		
	}

	private List<Long> getChavesEstrangeiras(ResultSet resultado, Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		List<Long> lista = new ArrayList<>();
		
		Object idValue = getMetodo(o.getClass(), br.com.waiso.framework.abstratas.TipoMetodo.GET, "id").invoke(o);
		
		long id;
		if (ObjectUtils.isEmpty(idValue)) {
			id = resultado.getLong(GeradorSQLBean.getInstancia(o.getClass()).getCampo(GeradorSQLBean.getInstancia(o.getClass()).getCampo("id"), false));
		} else {
			id = (Long) idValue;
		}
		
		Class tipoFK = campo.getType();
		FinderJDBC<?> finderFK = new FinderJDBC<>(tipoFK, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
		
		Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		
		String sql = GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoSelecao();
		finderFK.addFiltro(new Filtro(Long.class, o.getClass().getSimpleName().toLowerCase()+"_id", TipoFiltro.IGUAL_A, id, true));
		ResultSet resultados = null;
		DAOGenericoJDBCImpl<?> dao = new DAOGenericoJDBCImpl<>(o.getClass());
		try {
			resultados = finderFK.pesquisar(sql.toString(), finderFK.filtros);
		} catch (SQLSyntaxErrorException e) {
			if (e.getMessage().contains("Table ") && e.getMessage().contains("doesn't exist")) {
				String sqlCreate = GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoTabelaCriacao();
				dao.executarComando(sqlCreate.toString());
				resultados = finderFK.pesquisar(sql.toString(), finderFK.filtros);
			}
		}
		
		while (existe(resultados) && resultados.next()) {
			Long chave = resultados.getLong(classeGenerica.getSimpleName().toLowerCase()+"_id");
			lista.add(chave);
		}
		finderFK.fecharObjetosDeComandoEPesquisa(null, null, resultados);
		
		return lista;
	}
	
	public void incluir(Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o, null);
		Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(classeGenerica) {};
		for (Object item : listaParam) {
			//verificar se o itemFK ja existe ou cria-lo
			boolean existeFK = existeFK(item);
			Object objetoIncluido = null;
			if (!existeFK) {
				objetoIncluido = dao.incluir(item);
			} else {
				Long id = (Long) getMetodo(classeGenerica, TipoMetodo.GET, "id").invoke(item);
				objetoIncluido = new FinderJDBC<>(classeGenerica).pesquisar(id);
			}
			
			if(!existeRelacaoEntidadeFraca(o, objetoIncluido)) {
				//realizar a associacao do objeto encontrado/criado
				StringBuilder sqlBuilder = new StringBuilder(GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoInsercao());
				Object indiceIncluido = getMetodo(objetoIncluido.getClass(), TipoMetodo.GET, "id").invoke(objetoIncluido, null);
				dao.comandoPreparado = dao.novoComandoPreparado(sqlBuilder.toString());
				Long indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o, null);
				dao.comandoPreparado.setLong(1, indiceParametro);
				dao.comandoPreparado.setLong(2, (long) indiceIncluido);
				dao.executarOperacaoParametrizada(dao.comandoPreparado);
			}
		}
		
	}

	private boolean existeFK(Object item) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		Object objectId = getMetodo(item.getClass(), TipoMetodo.GET, "id").invoke(item);
		if (ObjectUtils.isNotEmpty(objectId)) {
			long id = (long) objectId; 
			Object bdItem = new FinderJDBC<>(item.getClass()).pesquisar(id);
			return ObjectUtils.isNotEmpty(bdItem);
		}
		return false;
	}
	
	private boolean existeRelacaoEntidadeFraca(Object o, Object objetoIncluido) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		FinderJDBC<?> finder = new FinderJDBC<>(objetoIncluido.getClass(), TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
		String sql = GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), objetoIncluido.getClass()).getComandoSelecao();
		Object id = getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
		Object idFK = getMetodo(objetoIncluido.getClass(), TipoMetodo.GET, "id").invoke(objetoIncluido);
		Filtro f = new Filtro(Long.class, o.getClass().getSimpleName()+"_id", TipoFiltro.IGUAL_A, id, true);
		Filtro f1 = new Filtro(Long.class, objetoIncluido.getClass().getSimpleName()+"_id", TipoFiltro.IGUAL_A, idFK, true);
		List<Filtro> filtros = new ArrayList<>();
		filtros.add(f);
		filtros.add(f1);
		ResultSet resultadoPesquisaRelacaoEntidadeFraca = finder.pesquisar(sql, filtros);
		return ObjectUtils.isNotEmpty(resultadoPesquisaRelacaoEntidadeFraca) && resultadoPesquisaRelacaoEntidadeFraca.next();
	}

	public void alterar(Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException, NoSuchFieldException {
		/*
		 * passo 1 - verificar se os ids das FKs ja existem na base
		 * if (!existe) {
		 * 		criar();
		 * }
		 * 
		 * passo 2 - verificar se as relacoes ja existem.
		 * if(!existe) {
		 * 		criar();
		 * }
		 */
		incluir(o, campo);
		
		
		/*
		 * passo 2 - verificar se as relacoes ainda existem.
		 * for (Entidade fraca: linhasEntidadeFraca){
		 * 		if (!listaParam.contains(fraca)) {
		 * 			excluir();
		 * 		}
		 * }
		 * 
		 */
		
		List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o, null);
		List<Long> idsParam = new ArrayList<>();
		for (Object object : listaParam) {
			Object objId = getMetodo(object.getClass(), TipoMetodo.GET, "id").invoke(object);
			if (ObjectUtils.isNotEmpty(objId)) {
				idsParam.add((Long)objId);
			}
		}
		Class<?> classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(classeGenerica) {};
		FinderJDBC<?> finder = new FinderJDBC<>(classeGenerica, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
		
		StringBuilder sqlBuilder = new StringBuilder(GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoSelecao());
		List<Filtro> filtros = new ArrayList<>();
		Object id = getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
		Filtro f = new Filtro(Long.class, o.getClass().getSimpleName()+"_id", TipoFiltro.IGUAL_A, id, true);
		filtros.add(f);
		
		ResultSet relacoesASeremExcluidas = finder.pesquisar(sqlBuilder.toString(), filtros);
		while (ObjectUtils.isNotEmpty(relacoesASeremExcluidas) && relacoesASeremExcluidas.next()) {
			filtros.clear();
			StringBuilder sql = new StringBuilder(GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoDelecao());
			Long idFK = relacoesASeremExcluidas.getLong(classeGenerica.getSimpleName()+"_id");
			if (!idsParam.contains(idFK)) {
				Class<?> tipoPK = getMetodo(classeGenerica, TipoMetodo.GET, "id").getReturnType();
				Filtro f1 = new Filtro(tipoPK, classeGenerica.getSimpleName()+"_id", TipoFiltro.IGUAL_A, idFK, true);
				filtros.add(f);
				filtros.add(f1);
				dao.excluir(sql.toString(), filtros);
			}
		}
		
//		for (Object object : listaParam) {
//			StringBuilder sqlBuilder = new StringBuilder(GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), classeGenerica).getComandoSelecao());
//			List<Filtro> filtros = new ArrayList<>();
//			Object id = getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
//			Filtro f = new Filtro(campo.getType(), classeGenerica.getSimpleName()+"_id", TipoFiltro.IGUAL_A, id, true);
//			filtros.add(f);
//			
//			ResultSet relacoesASeremExcluidas = finder.pesquisar(sqlBuilder.toString(), filtros);
//			Object indiceIncluido = getMetodo(relacoesASeremExcluidas.getClass(), TipoMetodo.GET, "id").invoke(object);
//			dao.comandoPreparado = dao.novoComandoPreparado(sqlBuilder.toString());
//			Long indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
//			dao.comandoPreparado.setLong(1, indiceParametro);
//			dao.comandoPreparado.setLong(2, (long) indiceIncluido);
//			dao.executarOperacaoParametrizada(dao.comandoPreparado);
//		}
		
		
	}

	public void excluir(Object o, Field campo) {
		// TODO Auto-generated method stub
		
	}

}
