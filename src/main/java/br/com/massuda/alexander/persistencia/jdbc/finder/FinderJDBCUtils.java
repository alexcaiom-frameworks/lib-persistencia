package br.com.massuda.alexander.persistencia.jdbc.finder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.waiso.framework.abstratas.TipoMetodo;

public class FinderJDBCUtils<T> extends DAOGenericoJDBCImpl<T> {
	
	public List<Filtro> filtros = new ArrayList<>();

	public FinderJDBCUtils(Class entidade) {
		super(entidade);
	}

	/**
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	protected T novaInstancia() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		return (T) entidade.getDeclaredConstructors()[0].newInstance();
//		return ((T)((ParameterizedType)entidade.getClass().getGenericSuperclass())
//				.getActualTypeArguments()[0]).;
	}

	protected void preencher(T o) throws SQLException {
		preencher(o, resultados);
	}
	
	protected void preencher(T o, ResultSet resultados) throws SQLException {
		Field[] campos = o.getClass().getDeclaredFields();

		List<Field> atributos = new ArrayList<Field>();
		List<Method> settersDosAtributos = new ArrayList<Method>();
		//Primeiro, verificamos os campos e se existe heranca
		captarMetodosGet(o, campos, atributos, settersDosAtributos);

		if (possuiHeranca) {
			addCamposHerdadosAListaDeMetodosSetters(o.getClass(), settersDosAtributos, atributos);
		}

		//Depois, iteramos os getters ja verificados e pegamos seus valores
		Method[] metodos = settersDosAtributos.toArray(new Method[settersDosAtributos.size()]);
		for (int i=0; i < metodos.length; i++) {
			Method metodo = metodos[i];
			Field campo = atributos.get(i);
			boolean anotado = GeradorSQLBeanFactory.get(entidade).ehChaveEstrangeira(campo);
			String nomeDoCampoNoBancoDeDados = anotado ? GeradorSQLBeanFactory.get(entidade).getCampo(campo, false) : campo.getName();
			if (!devePular(campo)) {
				try {
					extrator.getValorDaPesquisa(resultados, campo, nomeDoCampoNoBancoDeDados, metodo, o, anotado);
				} catch (IllegalAccessException | InvocationTargetException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (chaveEstrangeiraEstaEmOutroBD(campo)) {
				long fkId = resultados.getLong(GeradorSQLBeanFactory.get(o.getClass()).getCampo(campo, false));
				try {
					Object fk = campo.getType().newInstance();
					getMetodo(fk.getClass(), TipoMetodo.SET, "id").invoke(fk, fkId);
					getMetodo(o.getClass(), TipoMetodo.SET, campo.getName()).invoke(o, fk);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private boolean chaveEstrangeiraEstaEmOutroBD(Field campo) {
		return (campo.isAnnotationPresent(ChaveEstrangeira.class) 
				&& campo.getAnnotation(ChaveEstrangeira.class).estaEmOutraTabela());
	}

	private boolean devePular(Field campo) {
		return campo.isAnnotationPresent(Transiente.class)
				|| chaveEstrangeiraEstaEmOutroBD(campo)
				|| (campo.isAnnotationPresent(ChaveEstrangeira.class) &&
						campo.getAnnotation(ChaveEstrangeira.class).pular())
				;
	}

	private void captarMetodosGet(T o, Field[] campos, List<Field> atributos, List<Method> settersDosAtributos) {
		for (int i = 0; i < campos.length; i++) {
			Field campo  = campos[i];
			if (!GeradorSQLBean.campoDeveSerPersistido(campo)) { continue; }
			try {
				Method metodo = getMetodo(o.getClass(), TipoMetodo.SET, campo.getName());
				settersDosAtributos.add(metodo);
				atributos.add(campo);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	}

	private List getChavesEstrangeiras(T o, Class<?> tipoFK, Field atributo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		List lista = new ArrayList<>();
		
		long id = (long) getMetodo(o.getClass(), br.com.waiso.framework.abstratas.TipoMetodo.GET, "id").invoke(o, null);
		
		FinderJDBC finderFK = new FinderJDBC<>(tipoFK);
		
		Filtro filtro = new Filtro(atributo, TipoFiltro.IGUAL_A, id, true);
		addFiltro(filtro);
		lista = finderFK.pesquisar(filtros);
		
		return lista;
	}

	private void getChaveEstrangeira1Para1(ResultSet resultado, Class tipoCampo, String nome, String nomeBD, T o,
			Boolean anotado, boolean temAtributoId) throws InstantiationException, IllegalAccessException,
			NoSuchMethodException, SQLException, InvocationTargetException {
		try {
			//FIXME este trecho de Codigo nao cobre herancas profundas
			boolean temHeranca = GeradorSQLBean.possuiHeranca(tipoCampo);
			if (temHeranca) {
				Class mae = tipoCampo.getSuperclass();
				while(GeradorSQLBean.possuiHeranca(mae)){
					mae = mae.getSuperclass();
				}
				temAtributoId = existe(mae.getDeclaredField("id"));
			} else {
				temAtributoId = existe(tipoCampo.getDeclaredField("id"));
			}
			//FIXME este trecho de Codigo nao cobre herancas profundas
		}
		catch (NoSuchFieldException e) {
			e.printStackTrace(); 
		}
		catch (SecurityException e) {  
			e.printStackTrace();  
		}
		
		if (temAtributoId) {
			Object entidadeRelacionada = tipoCampo.newInstance();
			FinderJDBC finderFK = new FinderJDBC<>(tipoCampo); 
			
			Method metodoSetId = null;
			if (!possuiHeranca) {
				metodoSetId = entidadeRelacionada.getClass().getMethod("setId", Long.class);
			} else {
				Class mae = entidadeRelacionada.getClass().getSuperclass();
				while(GeradorSQLBean.possuiHeranca(mae)){
					mae = mae.getSuperclass();
				}
				metodoSetId = mae.getMethod("setId", Long.class);
			}
			String sufixo_id = (!anotado) ? "_id" : "";
			nomeBD = nomeBD.isEmpty() ? nome + sufixo_id : nomeBD;
			Long id = resultado.getLong(nomeBD);
			
			entidadeRelacionada = finderFK.pesquisar(id);
			metodoSetId.invoke(entidadeRelacionada, id);
			
			Method metodoSetEntidade = null;
			try {//Tentamos obter o setter da chave estrangeira. Se for o mesmo tipo de entidade, podemos usar o modo normal.
				metodoSetEntidade =  o.getClass().getMethod("set"+tipoCampo.getSimpleName(), tipoCampo);
			} catch (NoSuchMethodException eSemMetodo){
				metodoSetEntidade = o.getClass().getMethod("set"+nome.substring(0, 1).toString().toUpperCase()+nome.substring(1), tipoCampo);
			}
			
			metodoSetEntidade.invoke(o, entidadeRelacionada);
		}
	}
	
	public void addFiltro(Filtro filtro) {
		if (existe(filtro)) {
			filtros.add(filtro);
		}
	}
	
	public void limparFiltros() {
		filtros.clear();
	}

	public List<Filtro> getFiltros() {
		return filtros;
	}
	
	
}