/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

/**
 * @author Alex
 *
 */
public enum JDBCTypes {
	
	INTEIRO(ComunicacaoTipoInteiro.class, 	Integer.class, int.class),
	Long (	ComunicacaoTipoLong.class, 	Long.class, long.class),
	Double (ComunicacaoTipoDouble.class, 	Double.class, double.class),
	Float (	ComunicacaoTipoFloat.class, 	Integer.class, float.class),
	Boolean(ComunicacaoTipoBoolean.class, 	Integer.class, boolean.class, Boolean.class),
	String (ComunicacaoTipoString.class, 	String.class),
	Data (	ComunicacaoTipoData.class, 	Calendar.class, Date.class, LocalDate.class, LocalDateTime.class),
	Enum (	ComunicacaoTipoEnum.class, 	(Class[])null);
	
	public static final java.lang.String CHAVE_ESTRANGEIRA = "Chave Estrangeira";
	public static final java.lang.String TIPO_INVALIDO = "Tipo invalido";
	private Class extrator;
	private Class[] classes;

	private JDBCTypes(Class<? extends ComunicacaoJDBC> extrator,  Class... classes) {
		this.extrator = extrator;
		this.classes = classes;
	}

	public Class getExtrator() {
		return extrator;
	}

	public void setExtrator(Class<? extends ComunicacaoJDBC> extrator) {
		this.extrator = extrator;
	}

	public Class[] getClasses() {
		return classes;
	}

	public void setClasses(Class[] classes) {
		this.classes = classes;
	}
	
	public static Class get(Class tipo) {
		if (tipo.isEnum()) {
			return Enum.getExtrator();
		}
		for (JDBCTypes extrator : values()) {
			if (ObjectUtils.isNotEmpty(extrator.getClasses())) {
				for (Class tipoDisponivel : extrator.getClasses()) {
					if (tipoDisponivel.equals(tipo)) {
						return extrator.getExtrator();
					}
				}
			} else {
				throw new IllegalArgumentException(CHAVE_ESTRANGEIRA);
			}
		}
		throw new IllegalArgumentException(TIPO_INVALIDO);
	}
	
	public static ComunicacaoJDBC selecionar(List<ComunicacaoJDBC> extratores, Class tipo) {
		Class selecionado = get(tipo);
		for (ComunicacaoJDBC extrator : extratores) {
 			if ( selecionado.equals(extrator.getClass())) {
				return extrator;
			}
		}
		throw new IllegalArgumentException(TIPO_INVALIDO);
	}

}
