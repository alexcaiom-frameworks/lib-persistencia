/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author Alex
 *
 */
public class ComunicacaoTipoEnum extends ComunicacaoJDBC  {
	
	public ComunicacaoTipoEnum() {
		this.SQL_VALOR_NULO = Types.VARCHAR;
	}

	void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(existe(valor) ? ((Enum)valor).name() : null));		
	}

	void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado)
			throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String nameEnum = resultado.getString(campo.getName());
		Object valor = null;
		if (existe(nameEnum)) {
			Class tipoCampo = campo.getType();
			valor = Enum.valueOf(tipoCampo, nameEnum);
		}
		setter.invoke(o, valor);
		
//		setter.invoke(o, Enum.valueOf((Class<? extends Enum>) campo.getType(), resultado.getString(campo.getName())));
		
	}

}
