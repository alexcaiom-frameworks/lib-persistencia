/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;

/**
 * @author Alex
 *
 */
public class GeradorSQLBeanFactory {
	
	public static Map<Class<?>, GeradorSQLBean<?>> map = new HashMap<>();
	
	public static GeradorSQLBean get(Class<?> tipo) {
		if (!map.containsKey(tipo)) {
			map.put(tipo, GeradorSQLBean.getInstancia(tipo));
		}
		return map.get(tipo);
	}

}
