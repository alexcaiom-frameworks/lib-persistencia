/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.ObjectUtils;

/**
 * @author Alex
 *
 */
public class ComunicacaoTipoData extends ComunicacaoJDBC {

	public ComunicacaoTipoData() {
		this.SQL_VALOR_NULO = Types.DATE;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		java.sql.Date dataBD = null;
		if (tipo == Calendar.class) {
			Calendar data = (Calendar)valor;
			dataBD = setDataBD(data);
		} else if (tipo == Date.class) {
			Date data = (Date)valor;
			dataBD = setDataBD(data); 
		} else if (tipo == LocalDate.class) {
			LocalDate data = (LocalDate) valor;
			dataBD = setDataBD(data);
		} else if (tipo == LocalDateTime.class) {
			LocalDateTime hora = (LocalDateTime) valor;
			try {
				dataBD = setDataBD(hora);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		comandoPreparado.setDate(indice, dataBD);
	}
	
	private java.sql.Date setDataBD (Calendar data){
		return new java.sql.Date(data.getTimeInMillis());
	}
	
	private java.sql.Date setDataBD (Date data){
		return new java.sql.Date(data.getTime());
	}
	
	private java.sql.Date setDataBD (LocalDate data){
		return new java.sql.Date(data.toEpochDay());
	}
	
	private java.sql.Date setDataBD (LocalDateTime hora) throws ParseException{
		try {
			if (null != hora) {
				Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(hora.toString().replaceAll("T", " "));
				return new java.sql.Date(dt.getTime());
			}
			return null;
		} catch (ParseException e) {
			throw e;
		}
	}

	void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado)
			throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		java.sql.Date dataBD = resultado.getDate(campo.getName());
		Class tipo = campo.getType();
		Object data = null;
		if (ObjectUtils.isNotEmpty(dataBD)) {
			if (tipo == Calendar.class) {
				data = Calendar.getInstance();
				getDataBD(dataBD, (Calendar) data);
			} else if (tipo == Date.class) {
				data = new Date();
				getDataBD(dataBD, (Date)data);
			} else if (tipo == LocalDate.class) {
				data = LocalDate.now();
				getDataBD(dataBD, (LocalDate)data);
			} else if (tipo == LocalDateTime.class) {
				data = LocalDateTime.now();
				data = getDataBD(dataBD);
			}
			setter.invoke(o, data);
		}
	}
	
	private void getDataBD (java.sql.Date dataBD, Calendar data){
		data.setTimeInMillis(dataBD.getTime());
	}
	
	private void getDataBD (java.sql.Date dataBD, Date data){
		data.setTime(dataBD.getTime());
	}
	
	private void getDataBD (java.sql.Date dataBD, LocalDate data){
		data.withDayOfMonth(dataBD.getDay());
		data.withMonth(dataBD.getMonth());
		data.withYear(dataBD.getYear());
	}
	
	private LocalDateTime getDataBD (java.sql.Date dataBD) {
//		return LocalDateTime.of(dataBD.getYear(), dataBD.getMonth(), dataBD.getDay(), dataBD.getHours(), dataBD.getMinutes(), dataBD.getSeconds());
		return LocalDateTime.of(dataBD.getYear(), dataBD.getMonth(), dataBD.getDay(), 0, 0, 0);
	}

}
