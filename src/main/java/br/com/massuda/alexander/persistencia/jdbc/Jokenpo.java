/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc;

/**
 * @author Alex
 *
 */
public class Jokenpo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Movimento movimento1 = Movimento.Pedra;
		Movimento movimento2 = Movimento.Tesoura;
		Movimento movimento3 = Movimento.Spock;
		System.out.println("Ganhador: "+Regras.quemGanha(movimento1, movimento2));
		System.out.println("Ganhador: "+Regras.quemGanha(movimento1, movimento3));

	}

}
