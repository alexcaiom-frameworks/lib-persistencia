package br.com.massuda.alexander.persistencia.jdbc.anotacoes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <b>PT:</b> Campos marcados com essa anotacao nao serao persistidos/lidos<br/>
 * <b>EN:</b> Fields marked with this annotation will not be persisted/read
 * @author Alex
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Transiente {

	
}