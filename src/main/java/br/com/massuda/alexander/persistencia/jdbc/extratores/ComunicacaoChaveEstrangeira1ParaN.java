/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.persistencia.jdbc.model.Operacao;
import br.com.massuda.alexander.persistencia.jdbc.testes.dao.Finder;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.massuda.alexander.persistencia.jpa.DAOGenericoJPAImpl;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;

/**
 * @author Alex
 *
 */
public class ComunicacaoChaveEstrangeira1ParaN extends ComunicacaoJDBC {
	
	private static final String REGEX_PONTO = "\\.";

	public ComunicacaoChaveEstrangeira1ParaN() {
		this.SQL_VALOR_NULO = Types.VARCHAR;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(String) valor);
	}

	public void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<Long> listaEntidadeFraca = getChavesEstrangeiras(resultado, o, campo);
		setter.invoke(o, listaEntidadeFraca);
	}
	

	private List<Long> getChavesEstrangeiras(ResultSet resultado, Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		List<Long> lista = new ArrayList<>();
		
		Object idValue = getMetodo(o.getClass(), br.com.waiso.framework.abstratas.TipoMetodo.GET, "id").invoke(o);
		
		long id;
		if (ObjectUtils.isEmpty(idValue)) {
			id = resultado.getLong(GeradorSQLBean.getInstancia(o.getClass()).getCampo(GeradorSQLBean.getInstancia(o.getClass()).getCampo("id"), false));
		} else {
			id = (Long) idValue;
		}
		
		Class<?> tipoGenericoLista = ReflectionUtils.getTipoCampoGenerico(campo);
		FinderJDBC finderFK = new FinderJDBC<>(tipoGenericoLista);
		
		Filtro filtro = new Filtro(null, TipoFiltro.IGUAL_A, id, true);
		filtro.setAtributoNome(o.getClass().getSimpleName().toLowerCase() + "_id");
		finderFK.addFiltro(filtro);
		lista = finderFK.pesquisar(finderFK.filtros);
		
		return lista;
	}

}
