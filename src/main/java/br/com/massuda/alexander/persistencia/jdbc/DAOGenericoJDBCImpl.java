package br.com.massuda.alexander.persistencia.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.util.List;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.persistencia.jdbc.excecoes.RegistroJaExistenteException;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.exceptions.ErroUsuario;

public class DAOGenericoJDBCImpl<T> extends DAOGenericoJDBCImplUtils<T> implements IDAO<T> {
	
	public DAOGenericoJDBCImpl(Class<T> entidade) {
		this.entidade = entidade;
		this.possuiHeranca = GeradorSQLBean.possuiHeranca(entidade);
	}

	public T incluir (T o) throws ErroUsuario, SQLException {
		tipoCRUD = OperacaoCRUD.INSERCAO;
		sql = GeradorSQLBeanFactory.get(entidade).getComandoInsercao(o);
		try {
			comandoPreparado = novoComandoPreparado(sql, RETORNAR_ID_GERADO);
			
			preencherParametrosDeComandoDeInsercao(comandoPreparado, o);
			ResultSet retornoChavesGeradas = this.executarOperacaoERetornarChavesGeradas(comandoPreparado);
			if (temAtributoIdEditavel(o) && retornoChavesGeradas.next()) {
				Long chaveGerada = retornoChavesGeradas.getLong(1);
				setarId(o, chaveGerada, null);
				processarPossiveisChavesEstrangeiras(o);
			}
			retornoChavesGeradas.close();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new RegistroJaExistenteException(e);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			return incluir(o);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return o;
	}
	
	public T editar (T o) {
		sql = GeradorSQLBeanFactory.get(entidade).getComandoAtualizacao(o);
		try {
			comandoPreparado =  novoComandoPreparado(sql);
			preencherEdicoes(comandoPreparado, o);
			
			executarOperacaoParametrizada(comandoPreparado);
			processarPossiveisChavesEstrangeiras(o);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			return editar(o);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return o;
	}
	
	public void editar (String sql) {
		try {
			comandoPreparado =  novoComandoPreparado(sql);
			executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			editar(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	public void excluir(T o) throws ErroUsuario {
		if (Classe.naoExiste(o)) {
			throw new ErroUsuario("Objeto nao informado");
		}
		String sql = GeradorSQLBeanFactory.get(entidade).getComandoExclusao();
		sql += "\n where id = ?";
		try {
			comandoPreparado = novoComandoPreparado(sql);
			preencherExclusao(comandoPreparado, o);
			executarOperacaoParametrizada(comandoPreparado);
			processarPossiveisChavesEstrangeiras(o);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			excluir(o);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void excluir(long id) throws ErroUsuario {
		String sql = GeradorSQLBeanFactory.get(entidade).getComandoExclusao();
		sql += "\n where id = ?";
		log(sql);
		try {
			comandoPreparado = novoComandoPreparado(sql);
			comandoPreparado.setLong(1, id);
			executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			excluir(id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void excluir(EntidadeModelo objeto) throws ErroUsuario {
		verificaSeOModeloEstaPreenchido(objeto);
		excluir(objeto.getId());
	}

	public void incluir(String sqlEntidadeFraca, Object o, Object inserido) {
		tipoCRUD = OperacaoCRUD.INSERCAO;
		try {
			comandoPreparado = novoComandoPreparado(sqlEntidadeFraca);
			
			Long idFonte = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
			Long idChaveEstrangeira = (Long) getMetodo(inserido.getClass(), TipoMetodo.GET, "id").invoke(inserido);
			comandoPreparado.setLong(1, idFonte);
			comandoPreparado.setLong(2, idChaveEstrangeira);

			this.executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new RegistroJaExistenteException(e);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			incluir(sqlEntidadeFraca, o, inserido);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	

	public void excluir(String sqlEntidadeFraca, Object o, Object inserido) {
		tipoCRUD = OperacaoCRUD.EXCLUSAO;
		String sql = GeradorSQLBean.EntidadeFraca.getInstancia(o.getClass(), inserido.getClass()).getComandoDelecao();
		log(sql);
		try {
			comandoPreparado = novoComandoPreparado(sql);
			
			Long idFonte = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
			Long idChaveEstrangeira = (Long) getMetodo(inserido.getClass(), TipoMetodo.GET, "id").invoke(inserido);
			comandoPreparado.setLong(1, idFonte);
			comandoPreparado.setLong(2, idChaveEstrangeira);

			this.executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new RegistroJaExistenteException(e);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			excluir(sqlEntidadeFraca, o, inserido);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void excluir(String sqlEntidadeFraca, List<Filtro> filtros) {
		tipoCRUD = OperacaoCRUD.EXCLUSAO;
		try {
			sqlEntidadeFraca += parametrizarPesquisa(filtros);
			comandoPreparado = novoComandoPreparado(sqlEntidadeFraca);
			preencherParametrosDeComandoComFiltros(comandoPreparado, filtros);

			this.executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLIntegrityConstraintViolationException  e) {
			throw new RegistroJaExistenteException(e);
		} catch (SQLSyntaxErrorException e) {
			criarTabela(e);
			excluir(sqlEntidadeFraca, filtros);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
