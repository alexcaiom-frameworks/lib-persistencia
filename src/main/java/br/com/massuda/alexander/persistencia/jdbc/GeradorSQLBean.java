package br.com.massuda.alexander.persistencia.jdbc;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.sun.istack.NotNull;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChavePrimaria;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Coluna;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Tabela;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.Transiente;
import br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia;
import br.com.massuda.alexander.persistencia.jdbc.utils.MetodoDeEntidade;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.abstratas.TipoMetodo;

/**
 * Esta classe realiza a leitura do Bean passado
 * e cria as queries de criação de entidade baseado
 * nos atributos dela.
 * @author Alex
 *
 */
public class GeradorSQLBean<T> extends Classe {

	private static final String CHAVE_ESTRANGEIRA_MYSQL = "FOREIGN KEY";
	private String tabela = ""; 
	private static final String CREATE_TABLE_SIMPLES = "create table ";
	private static final String DROP_TABLE_SIMPLES = "drop table ";
	public static final String INSERT_INTO_SIMPLES = "insert into ";
	public static final String UPDATE_SIMPLES = "update ";
	public static final String DELETE_FROM_SIMPLES = "delete from ";
	public static final String SELECT_FROM_SIMPLES = "select * from ";

	private static final String CREATE_DATABASE = "create database  ";
	private final Class<?> classeObjeto;

	private final List<Field> camposDML = new ArrayList<Field>();
	private final List<Field> campos = new ArrayList<Field>();
	private final List<Field> camposPK = new ArrayList<Field>();
	private final List<Field> camposFKLista = new ArrayList<Field>();
	private final List<Field> camposFKObjeto = new ArrayList<Field>();
	private boolean possuiHeranca = false;

	/**
	 * 
	 * @param classeObjeto
	 */
	GeradorSQLBean(Class<?> classeObjeto){
		this.classeObjeto = classeObjeto;
		if(this.classeObjeto != null){
			//Primeiro pegamos o nome da Tabela
			tabela = getNomeTabela();
			
			for (int indice = 0; indice < this.classeObjeto.getDeclaredFields().length; indice++) {
				if (!"serialVersionUID".equals(this.classeObjeto.getDeclaredFields()[indice].getName())
					&& !this.classeObjeto.getDeclaredFields()[indice].isAnnotationPresent(Transiente.class)) {
					Field campo = this.classeObjeto.getDeclaredFields()[indice];
					campos.add(campo);
					verificarCampoComChave(campo);
				}
			}
			possuiHeranca = possuiHeranca(this.classeObjeto);
			if (possuiHeranca) {
				addCamposHerdados(this.classeObjeto);
			}
		}
	}
	
	void verificarCampoComChave(Field campo) {
		boolean temAnotacaoDeChavePrimaria = existe(campo.getAnnotation(ChavePrimaria.class));
		if(temAnotacaoDeChavePrimaria){
			camposPK.add(campo);
		}
		Class<?> tipo = campo.getType();
		if (ehLista(tipo) && !campoEhTransiente(campo)) {
			camposFKLista.add(campo);
		}
		if (!tipo.getCanonicalName().contains("java") && !tipo.isEnum() && !tipo.isPrimitive()) {
			camposFKObjeto.add(campo);
		}
	}

	/**
	 * @param tipo
	 * @return
	 */
	protected boolean ehLista(Class<?> tipo) {
		return List.class.equals(tipo) || ArrayList.class.equals(tipo);
	}
	
	public Field getCampo(String nome) {
		if (StringUtils.isEmpty(nome)) {
			return null;
		}
		for (Field campo : campos) {
			if (campo.getName().equals(nome)) {
				return campo;
			}
		}
		return null;
	}


	/**
	 * @param comando
	 * @param fks
	 * @param campo
	 * @param anotacoes
	 * @param tipoCampo
	 */
	public String getCampo(Field campo, boolean completo){
		StringBuilder txtcampo = new StringBuilder();
		if(!campo.getName().contains("serialVersionUID")){
			String tipoCampo = campo.getType().getName();

			boolean pegarCampoNormal = ehCampoNormal(campo, tipoCampo);

			/** Verificamos se o tipo do campo comeca com 'com',
			 * um indicativo de que estamos lidando com uma chave
			 * Estrangeira (FK)
			 */
			if(pegarCampoNormal){
				txtcampo.append(getCampoNormal(campo).trim());
				if (completo) {
					txtcampo.append(" ").append(getTipoCampo(campo).trim()).append(" ").append(getAtributosDeAnotacoes(campo.getAnnotations()).trim());
				}
			}else {
				txtcampo.append(getCampoChaveEstrangeira(campo).trim());
				if (completo) {
					txtcampo.append(" ").append(getTipoCampo(campo).trim()).append(" ").append(getAtributosDeAnotacoes(campo.getAnnotations()).trim());
				}
			}
		}

		return txtcampo.toString();
	}

	/**
	 * @param campo
	 * @param ehChaveEstrangeira
	 * @param tipoCampo
	 * @return
	 */
	private boolean ehCampoNormal(Field campo, String tipoCampo) {
		return (!campoEhChaveEstrangeira(campo)
				&& !List.class.equals(campo.getType()) && !Map.class.equals(campo.getType())
				&& (campo.getAnnotations().length == 0 || !campo.isAnnotationPresent(ChaveEstrangeira.class)) )
				|| campo.getType().isPrimitive();
	}

	public String getCampoNormal(Field campo){
		StringBuilder txtcampo = new StringBuilder();
		boolean campoTemAnotacaoColuna = campo.isAnnotationPresent(Coluna.class);
		if (campoTemAnotacaoColuna) {
			Coluna coluna = campo.getAnnotation(Coluna.class);
			return coluna.nome();
		}
		/**
		 * No caso de Chave Estrangeira, fazemos a extra��o do ID
		 * do tipo informado
		 */
		txtcampo.append(campo.getName());
		return txtcampo.toString();
	}
	private String getCampoChaveEstrangeira(Field campo){
		StringBuilder txtcampo = new StringBuilder();
		if (campo.isEnumConstant()) {
			return campo.getName();
		}
		boolean campoTemAnotacaoChaveEstrangeira = campo.isAnnotationPresent(ChaveEstrangeira.class);
		if (campoTemAnotacaoChaveEstrangeira && !StringUtils.isBlank(campo.getAnnotation(ChaveEstrangeira.class).nome())) {
			ChaveEstrangeira chaveEstrangeira = campo.getAnnotation(ChaveEstrangeira.class);
			return (!"NULL".equals(chaveEstrangeira.nome()) ? chaveEstrangeira.nome() : campo.getName());
		}
		/**
		 * No caso de Chave Estrangeira, fazemos a extra��o do ID
		 * do tipo informado
		 */
		if((campo.getType().isInterface() && (campo.getType().getSimpleName().equalsIgnoreCase("List") || 
				campo.getType().getSimpleName().equalsIgnoreCase("List"))) || campo.getType().getName().contains("com")
				|| ehChaveEstrangeira(campo)){
			txtcampo.append(campo.getName()).append("_id ");
		} else 
			return campo.getName();
		return txtcampo.toString();
	}

	/**
	 * Campo que interpreta a chave estrangeira,
	 * construindo a constraint
	 * @param campo
	 * @return
	 */
	private String getChaveEstrangeira(Field campo){
		if(campo != null){
			StringBuilder constraint = new StringBuilder();
			/**
			 * Se o nosso campo tiver implementado uma Interface com Generics,
			 * detectamos aqui e j� extraimos o tipo do campo.
			 */

			String tipoCampo = campo.getType().getSimpleName();

			constraint.append(CHAVE_ESTRANGEIRA_MYSQL + " fk_").append(tipoCampo.toLowerCase()).append("_").append(GeradorSQLBean.getInstancia(campo.getType()).getNomeTabela());
			constraint.append(" (").append(getCampoChaveEstrangeira(campo)).append(") references ").append(GeradorSQLBean.getInstancia(campo.getType()).getNomeTabela());
			constraint.append(" (id)");

			return constraint.toString();
		}
		return "";
	}

	/**
	 * Metodo que transforma os tipos de dados
	 * do Java em tipos de dado de Banco
	 * @param campo
	 * @return
	 */
	private String getTipoCampo(Field campo) {
		Class<?> classeTipo = null;
		if(!campo.getType().isPrimitive()){
			classeTipo = campo.getType();
			if(classeTipo == String.class){
				StringBuilder tipoBD = new StringBuilder("VARCHAR");
				Integer tamanho = getTamanhoDoCampo(campo);
				tipoBD.append("(").append(tamanho.toString()).append(")");
				return tipoBD.toString();
			} else if(classeTipo == Long.class){
				return "BIGINT";
			} else if(classeTipo == Boolean.class || classeTipo == Integer.class){
				return "INTEGER";
			} else if(classeTipo.isEnum()){
				StringBuilder tipoBD = new StringBuilder("VARCHAR");
				Integer tamanho = getTamanhoDoCampo(campo);
				tipoBD.append("(").append(tamanho.toString()).append(")");
				return tipoBD.toString();
			} else if(classeTipo.getName().contains("com.")){
				return "BIGINT";
			} else if ((classeTipo == LocalDate.class)) {
				return "DATE";
			} else if ((classeTipo == Date.class) || (classeTipo == Calendar.class) || (classeTipo == LocalDate.class) || (classeTipo == LocalDateTime.class)) {
				return "DATETIME";
			} else if (classeTipo == BigDecimal.class) {
				return "DECIMAL";
			}
		} else {
			if(!campo.getName().contains("serialVersionUID")){
				if(campo.getType().getSimpleName().equalsIgnoreCase(long.class.getSimpleName()) ||
						campo.getType().getSimpleName().equalsIgnoreCase(boolean.class.getSimpleName()) ||
						campo.getType().getSimpleName().equalsIgnoreCase(int.class.getSimpleName())){
					return "INTEGER";
				} else {
					return campo.getType().getSimpleName();
				}
			}
		}
		return "";
	}

	private int getTamanhoDoCampo(Field campo) {
		if (campo.isAnnotationPresent(Coluna.class) && null != campo.getAnnotation(Coluna.class)) {
			Coluna coluna = campo.getAnnotation(Coluna.class);
			return coluna.tamanho();
		}
		return ConstantesPersistencia.BANCO_DE_DADOS_TIPO_DADO_STRING_TAMANHO_PADRAO;
	}

	/**
	 * Caso o Bean tenha anota��es, estas s�o interpretadas
	 * para atributos de coluna no banco de dados.
	 * @param anotacoes
	 * @return
	 */
	private String getAtributosDeAnotacoes(Annotation[] anotacoes) {
		StringBuilder sb = new StringBuilder();
		String pk = "";
		StringBuilder nullable = new StringBuilder("");
		String auto_incremento = "";
		String unique = "";

		if(anotacoes != null && anotacoes.length > 0){
			for(Annotation anotacao: anotacoes){
				if(anotacao instanceof Coluna){
					Coluna a = (Coluna) anotacao;
					if(!a.anulavel()){
						nullable.insert(0, " not null");
					}
					/*if(a.generatedId()){

					}*/
					if(a.unica()){
						unique = " unique";
					}
				} else if (anotacao instanceof ChavePrimaria) {
					if (((ChavePrimaria)anotacao).autoIncremental()) {
						auto_incremento =" AUTO_INCREMENT"; 
					}
					pk = " primary key";

				}
			}
		}

		sb.append(pk).append(nullable.toString()).append(auto_incremento).append(unique);

		return sb.toString();
	}

	public String getComandoTabelaCriacao() {
		//Iniciando o comando
		StringBuilder comando = getComandoInicial(CREATE_TABLE_SIMPLES).append(" (");

		//Colocando os nomes dos campos
//		System.out.println("Campos: ");
		List<Field> campos = new ArrayList<>(Arrays.asList(classeObjeto.getDeclaredFields()));
		campos.addAll(addCamposHerdadosALista(classeObjeto));
		
		List<Field> fks = new ArrayList<Field>();
		/**
		 * 1a Parte - Cuidamos dos campos
		 */
		int quantidadeDeCampos = 0;
		for(Field campo: campos){

			if(!campo.getName().equals("serialVersionUID") 
					&& !getCampo(campo, false).trim().isEmpty()
					&& !getCamposFKLista().contains(campo)
					&& !campoEhTransiente(campo)){
//				String nomeCampo = campo.getName();
//				String tipoCampo = campo.getType().getName();
//				System.out.println(nomeCampo + " (" + tipoCampo + ")");

				StringBuilder linhaCampo = new StringBuilder();

				
				linhaCampo.append(getCampo(campo, true));

				if(!linhaCampo.toString().trim().isEmpty()){
					if(quantidadeDeCampos > 0){
						comando.append(", \n");
					} else{
						comando.append(" \n");
					}
					comando.append(linhaCampo.toString());
				}

				/** Verificamos se o tipo do campo come�a com 'com',
				 * um indicativo de que estamos lidando com uma chave
				 * Estrangeira (FK)
				 */
				if(campoEhChaveEstrangeira(campo)){
					fks.add(campo);
				}

				quantidadeDeCampos++;
			}
		}

		/**
		 * 2a Parte: Agora lidamos com as FKs
		 */
		for(Field f: fks){
			comando.append(",\n").append(getChaveEstrangeira(f));
		}

		comando.append(")");

		return comando.toString();
	}

	private boolean campoEhTransiente(Field campo) {
		return campo.isAnnotationPresent(Transiente.class);
	}

	boolean campoEhChaveEstrangeira(Field campo) {
		return !campo.getType().getCanonicalName().startsWith("java")
				&& GeradorSQLBean.getInstancia(campo.getType()).deveCriarTabela()
				&& !campo.getType().isEnum()
				&& !campo.getType().isPrimitive();
	}

	public String getComandoTabelaExclusao(){
		return getComandoInicial(DROP_TABLE_SIMPLES).toString();
	}

	public String getComandoInsercao(Object o){
		StringBuilder comando = new StringBuilder(getComandoInicial(INSERT_INTO_SIMPLES));

		//Primeiro, cuidamos de pegar todas as colunas
		comando.append(" (");

		List<MetodoDeEntidade> gettersDosAtributos = new ArrayList<>();
		boolean primeiroCampo = true;
		for (Field campo : getCamposDML()) {
			boolean ehChavePrimaria = ehChavePrimaria(campo);
			boolean ehChaveEstrangeira = ehChaveEstrangeira(campo);
			if (ehChavePrimaria && ehChavePrimariaAutoIncremental(campo)
					|| camposFKLista.contains(campo) || ehChaveEstrangeira) {
				continue;
			} 

			//Verifica se o atributo corrente esta preenchido
			MetodoDeEntidade metodo = null;
			try {
				Method metodoGet = getMetodo(o.getClass(), TipoMetodo.GET, campo.getName());
				
				metodo = new MetodoDeEntidade(ehChavePrimaria, ehChaveEstrangeira, metodoGet);
				metodo.setCampoDoMetodo(campo);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}

			if (!primeiroCampo) {
				comando.append(", ");
			}

			if (ehChaveEstrangeira) {
				comando.append(getCampoChaveEstrangeira(campo));
			} else {
				comando.append(getCampoNormal(campo));
			}
			primeiroCampo = false;

			gettersDosAtributos.add(metodo);
		}


		comando.append(") ");
		comando.append("values");

		//Entao, pegamos seus respectivos valores
		comando.append(" (");
		//Neste passo, pegamos os getters
		primeiroCampo = true;
		for (int i=0; i < gettersDosAtributos.size(); i++) {
			boolean ehChavePrimaria = ehChavePrimaria(gettersDosAtributos.get(i).getCampoDoMetodo());
			if (ehChavePrimaria) {
				if (ehChavePrimariaAutoIncremental(gettersDosAtributos.get(i).getCampoDoMetodo())) {
					continue;
				}
			}
			if (!primeiroCampo) {
				comando.append(", ");
			}
			comando.append("?");
			primeiroCampo = false;
		}
		comando.append(") ");

		return comando.toString();
	}


	public boolean ehChavePrimariaAutoIncremental(Field campo) {
		ChavePrimaria anotacao = null;
		if (ehChavePrimaria(campo)) {
			anotacao = campo.getAnnotation(ChavePrimaria.class);
		}
		return existe(anotacao) && anotacao.autoIncremental();
	}

	public boolean ehChavePrimaria(Field campo) {
		return campo.isAnnotationPresent(ChavePrimaria.class);
	}

	public boolean ehChaveEstrangeira(Field campo) {
//		boolean temAnotacaoChaveEstrangeira = campo.isAnnotationPresent(ChaveEstrangeira.class);
//		if (temAnotacaoChaveEstrangeira) {
//			ChaveEstrangeira chaveEstrangeira = campo.getAnnotation(ChaveEstrangeira.class);
//			return !chaveEstrangeira.estaEmOutraTabela(); 
//		}
		String tipoCampo = campo.getType().getName();
		return (!campo.getType().isPrimitive() && !tipoCampo.startsWith("java") || campoEhChaveEstrangeira(campo) || tipoCampo.startsWith("java.util")) 
				&& !campo.getType().isEnum() 
				&& campo.getType() != Calendar.class 
				&& campo.getType() != Date.class;
	}
	
	public boolean temChavesEstrangeirasListas() {
		return !getChavesExtrangeiras().isEmpty();
	}
	
	public boolean temChavesEstrangeirasObjetos() {
		return !this.getChavesExtrangeirasObjetos().isEmpty();
	}
	
	private List<Field> getChavesExtrangeiras() {
		return this.camposFKLista;
	}

	private List<Field> getChavesExtrangeirasObjetos() {
		return this.camposFKObjeto;
	}

	public String getComandoAtualizacao(Object o){
		if (naoExiste(o)) {
			return StringUtils.EMPTY;
		}
		String comando = getComandoInicial(UPDATE_SIMPLES).toString();
		boolean primeiroCampo = true;
		StringBuilder parametros = new StringBuilder();
		Field[] arrayCampos = campos.toArray(new Field[campos.size()]);
		
		for (int i = 0; i < arrayCampos.length; i++) {
			boolean chavePrimaria = ehChavePrimaria(arrayCampos[i]);
			boolean chaveEstrangeira = ehChaveEstrangeira(arrayCampos[i]);
			if (chaveEstrangeira) {
				continue;
			}
			
			if (!(chavePrimaria || chaveEstrangeira)) {
				if (primeiroCampo) {
					parametros.append("\nset ");
				} else {
					parametros.append(", \n");
				}
				String nomeCampo = getCampoNormal(arrayCampos[i]);
				parametros.append(nomeCampo).append(" = ?");
			}/* else if (chaveEstrangeira) {
				parametros.append(getCampoChaveEstrangeira(campos[i])).append(" = ?");
			}*/
			primeiroCampo = false;
		}
		
		

		StringBuilder filtro = new StringBuilder("\nwhere ");
		for (Field pks : camposPK) {
			boolean primeiraPK = true;
			if (!primeiraPK) {
				filtro.append("\n and ");
			}

			filtro.append(getCampoNormal(pks)).append(" = ?");
			primeiraPK = false;
		}

		return comando + parametros.toString() + filtro.toString();
	}

	public String getComandoExclusao(){
		String comando = getComandoInicial(DELETE_FROM_SIMPLES).toString();
		return comando;
	}

	public String getComandoSelecao(){
		String comando = getComandoInicial(SELECT_FROM_SIMPLES).toString();
		return comando;
	}
	
	public String getComandoSelecaoEntidadeFraca(Field campo){
		String comando = getComandoInicial(SELECT_FROM_SIMPLES).toString();
		return comando;
	}

	/**
	 * @return
	 */
	public String getNomeTabela() {
		boolean temAnotacaoDeTabela = existe(classeObjeto.getAnnotation(Tabela.class));
		if (temAnotacaoDeTabela) {
			Tabela t = (Tabela) classeObjeto.getAnnotation(Tabela.class);
			tabela = t.nome();
		} else {
			tabela = "tb_" + classeObjeto.getSimpleName().toLowerCase(new Locale("pt", "BR"));
		}
		return tabela;
	}


	
	private List<Field> addCamposHerdadosALista(Class<?> classeDoObjetoFilho) {
		List<Field> atributos = new ArrayList<>();
		Class<?> mae = classeDoObjetoFilho.getSuperclass();
		
		if (existe(mae)) {
			for (Field campoMae : mae.getDeclaredFields()) {
				atributos.add(campoMae);
			}
			
			if (possuiHeranca(mae)) {
				atributos.addAll(addCamposHerdadosALista(mae));
			}
		}
		return atributos;
	}

	private void addCamposHerdados(Class<?> classeDoObjetoFilho) {
		Class<?> mae = classeDoObjetoFilho.getSuperclass();

		if (existe(mae)) {
			for (Field campoMae : mae.getDeclaredFields()) {
				if (!"serialVersionUID".equals(campoMae.getName())) {
					campos.add(campoMae);
					verificarCampoComChave(campoMae);
				}
			}

			if (possuiHeranca(mae)) {
				addCamposHerdados(mae);
			}
		}
	}

	public static boolean possuiHeranca(Class<?> classeASerVerificada) {
		return existe(classeASerVerificada.getSuperclass()) && (classeASerVerificada.getSuperclass() != Object.class);
	}

	public static boolean campoDeveSerPersistido(Field campo) {
		if ("serialVersionUID".equals(campo.getName())) return false;
		if (naoExiste(campo.getAnnotation(Coluna.class))) {
			return true;
		} else {
			return campo.getAnnotation(Coluna.class).deveSerPersistida(); 
		}
	}

	public StringBuilder getComandoInicial(String qualComando){
		StringBuilder sb = new StringBuilder(qualComando);
		sb.append(getNomeTabela());
		return sb;
	}

	/**
	 * Obtenha uma instancia do Gerador de SQL para trabalhar com a classe parametro informada (Tipo)
	 * @param tipo
	 * @return new GeradorSQLBean
	 */
	public static <T> GeradorSQLBean<T> getInstancia(Class<T> tipo){
		return new GeradorSQLBean<T>(tipo);
	}

	public static void main(String[] args) {
		//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getCreateTable());
		//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getDropTable());
		//		System.out.println(GeradorSQLBean.getInstancia(Usuario.class).getInsert());
	}

	public List<Field> getCamposPK() {
		return camposPK;
	}
	
	public List<Field> getListaCampos() {
		return campos;
	}
	
	public Field[] getCampos() {
		return campos.toArray(new Field[campos.size()]);
	}

	public List<Field> getCamposFKLista() {
		return camposFKLista;
	}
	
	public List<Field> getCamposFKObjeto() {
		return camposFKObjeto;
	}
	
	public Field[] getCamposDML() {
		if (ObjectUtils.isEmpty(camposDML)) {
			for (Field campo : campos) {
				if (!camposFKLista.contains(campo)) {
					camposDML.add(campo);
				}
			}
		}
		return camposDML.toArray(new Field[camposDML.size()]);
	}

	public static String criarBD(String nome) {
		return CREATE_DATABASE.concat(nome);
	}

	public boolean deveCriarTabela() {
		boolean temAnotacaoTabela = classeObjeto.isAnnotationPresent(Tabela.class);
		if (temAnotacaoTabela) {
			Tabela anotacaoTabela = (Tabela) classeObjeto.getAnnotation(Tabela.class);
			return anotacaoTabela.criar();
		}
		return true;
	}

	public static class EntidadeFraca {

		
		private Class<? extends Object> origem;
		private Class<?> destino;
		
		public EntidadeFraca(@NotNull Class<? extends Object> origem, @NotNull Class<?> destino) {
			this.origem = origem;
			this.destino = destino;
		}
		public static EntidadeFraca getInstancia(Class<? extends Object> class1, Class<?> classeGenerica) {
			return new EntidadeFraca(class1, classeGenerica);
		}
		public String getComandoSelecao() {
			String nomeTabela = getNomeTabela();
			return new StringBuilder(GeradorSQLBean.SELECT_FROM_SIMPLES).append(nomeTabela).toString();
		}
		public String getComandoInsercao() {
			String nomeTabela = getNomeTabela();
			StringBuilder sql = new StringBuilder(GeradorSQLBean.INSERT_INTO_SIMPLES).append(nomeTabela);
			sql.append(" (")
							.append(origem.getSimpleName().toLowerCase()).append("_id, ")
							.append(destino.getSimpleName().toLowerCase()).append("_id")
				.append(")")
				.append(" values (?, ?)");
							;
			return sql.toString();
		}
		private String getNomeTabela() {
			String nomeOrigem = GeradorSQLBean.getInstancia(origem).getNomeTabela().replace("tbl_", "");
			String nomeDestino = GeradorSQLBean.getInstancia(destino).getNomeTabela().replace("tbl_", "");
			return new StringBuilder("tbl_").append(nomeOrigem).append("_").append(nomeDestino).toString();
		}
		public String getComandoDelecao() {
			String nomeTabela = getNomeTabela();
			StringBuilder sql = new StringBuilder(GeradorSQLBean.DELETE_FROM_SIMPLES).append(nomeTabela);
							;
			return sql.toString();
		}
		public String getComandoTabelaCriacao() {
			String nomeTabela = getNomeTabela();
			StringBuilder sql = new StringBuilder(GeradorSQLBean.CREATE_TABLE_SIMPLES).append(nomeTabela);
			sql.append(" (")
				.append(origem.getSimpleName().toLowerCase()).append("_id ").append("INTEGER, ")
				.append(destino.getSimpleName().toLowerCase()).append("_id ").append("INTEGER ")
				.append(")");
			return sql.toString();
		}
		
	}

	public boolean tabelaEstaNoMesmoBD(Field campo) {
		if (campo.isAnnotationPresent(ChaveEstrangeira.class)) {
			ChaveEstrangeira chave = campo.getAnnotation(ChaveEstrangeira.class);
			return !chave.estaEmOutraTabela();
		}
		return true;
	}
}