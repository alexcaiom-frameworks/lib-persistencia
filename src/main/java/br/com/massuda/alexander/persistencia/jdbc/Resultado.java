package br.com.massuda.alexander.persistencia.jdbc;

public class Resultado {
	
	public int resultado;
	public String mensagem;
	
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	@Override
	public String toString() {
		return "Resultado [resultado=" + resultado + ", mensagem=" + mensagem + "]";
	}
}